# KDE Applications Menu Mockup

This is a quick sketch of a startup menu design. Enjoy [the video](/kde_plasma_startup_mockup.webm)! :·)

Some things have been improved since the video has been made concerning the readability.
![The Startup Menu](/Menu.png)



## Feedback is welcome !

**Don't hesitate to create issues for things you would like to change or ideas you want to add! :)**

I'll read them and everyone is welcome to add and discuss them!

The aim is to improve and work out the rough edges of the mockup to get everything layed out for creating it if it gets enough momentum.
So even just giving your opinion on it is already helpful!

Live conversation happens per IRC on freenode, channel *#kde-vdg*, or on the telegram room of kdes visual design group : *[https://t.me/vdgmainroom](https://t.me/vdgmainroom)*





I had very little time lately, so the video is a bit rough, sorry.